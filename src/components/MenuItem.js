export const MenuItem = [
  {
    title: "Home",
    url: "/",
    clName: "nav-links",
    icon: "fa-solid fa-house",
  },

  {
    title: "Destinations",
    url: "/destinations",
    clName: "nav-links",
    icon: "fa-solid fa-car",
  },

  {
    title: "Foreign Trips",
    url: "/trips",
    clName: "nav-links",
    icon: "fa-solid fa-plane",
  },

  {
    title: "About",
    url: "/about",
    clName: "nav-links",
    icon: "fa-solid fa-circle-info",
  },

  {
    title: "Contact Us",
    url: "/contact",
    clName: "nav-links",
    icon: "fa-solid fa-address-book",
  },

  {
    title: "Sign Up",
    url: "/signup",
    clName: "nav-links-mobile",
  },
];
