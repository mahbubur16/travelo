import "./Aboutus.css";

function Aboutus() {
  return (
    <div className="abt">
      <h1>Our History</h1>
      <p>
        Founded in 2005 by passionate travelers with a shared love for
        exploration, Travelo began as a small venture aimed at providing
        personalized travel experiences to adventurers worldwide. Over the
        years, we have grown into a renowned name in the travel industry,
        committed to delivering exceptional service, inspiring journeys, and
        unforgettable memories to our valued customers.
      </p>

      <h1>Our Mission</h1>
      <p>
        Our mission at Travelo is to curate extraordinary travel experiences
        that exceed expectations and create lasting impressions. We are
        dedicated to offering unparalleled customer service, ensuring seamless
        journeys from planning to completion. Through responsible tourism
        practices, we aim to contribute positively to the destinations we visit,
        preserving natural wonders and supporting local communities.
      </p>

      <h1>Our Vision</h1>
      <p>
        At Travelo, our vision is to become the leading global travel company
        that empowers people to explore the world with confidence, curiosity,
        and a deep appreciation for diverse cultures. We aspire to be pioneers
        in sustainable travel practices, continuously innovating to enhance
        customer experiences, and setting the benchmark for excellence in the
        industry.
      </p>
    </div>
  );
}
export default Aboutus;
